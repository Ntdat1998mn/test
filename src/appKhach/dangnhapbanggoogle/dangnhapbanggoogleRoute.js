const express = require("express");
const { googleLogin, callBack } = require("./dangnhapbanggoogle");

const dangnhapbanggoogleRoute = express.Router();

// Đăng Nhập
dangnhapbanggoogleRoute.get("/auth/google/login", googleLogin);
dangnhapbanggoogleRoute.get("/auth/google/login/callback", callBack);

module.exports = dangnhapbanggoogleRoute;

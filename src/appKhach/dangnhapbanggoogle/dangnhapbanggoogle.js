const { google } = require("googleapis");

// Cấu hình thông tin xác thực OAuth 2.0
const credentials = {
  client_id:
    "601966078436-sar7hg1c5g551gtdgf3mu9mf0eoidfp6.apps.googleusercontent.com",
  client_secret: "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",
  redirect_uri:
    "http://159.223.86.73:3010/api/app-khach/user/auth/google/login/callback",
};

const googleLogin = async (req, res) => {
  // Tạo một phiên OAuth2Client từ thông tin xác thực
  const oauth2Client = new google.auth.OAuth2(
    credentials.client_id,
    credentials.client_secret,
    credentials.redirect_uri
  );

  // Tạo URL xác thực để chuyển hướng người dùng đến
  const authUrl = oauth2Client.generateAuthUrl({
    scope: ["profile", "email"], // Quyền truy cập thông tin hồ sơ và email
  });
  console.log("authUrl: ", authUrl);

  // Chuyển hướng người dùng đến URL xác thực
  res.redirect(authUrl);
};

// Định nghĩa tuyến đường để xử lý khi người dùng đã xác thực và trả về mã truy cập
const callBack = async (req, res) => {
  const oauth2Client = new google.auth.OAuth2(
    credentials.client_id,
    credentials.client_secret,
    credentials.redirect_uri
  );

  // Lấy mã truy cập từ tham số truy vấn
  const code = req.query.code;

  try {
    // Đổi mã truy cập lấy được thành token truy cập
    const { tokens } = await oauth2Client.getToken(code);

    // Sử dụng token truy cập để xác thực và lấy thông tin người dùng
    oauth2Client.setCredentials(tokens);
    const oauth2 = google.oauth2({ version: "v2", auth: oauth2Client });
    const userInfo = await oauth2.userinfo.get();

    // Ở đây, bạn có thể lưu thông tin người dùng vào cơ sở dữ liệu hoặc thực hiện các tác vụ khác
    console.log("User Info:", userInfo.data);

    // Trả về phản hồi cho người dùng
    res.send("Đăng nhập thành công!");
  } catch (error) {
    console.error("Lỗi xác thực:", error);
    res.status(500).send("Đăng nhập không thành công!");
  }
};

module.exports = { googleLogin, callBack };

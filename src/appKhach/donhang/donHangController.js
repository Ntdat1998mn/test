const {PrismaClient} =   require('@prisma/client') 
const prisma = new PrismaClient()

//  Lấy danh sách đơn hàng
let getDonHang = async(req,res) => {
    try{
        let user_id = req.user.user_id;
        let donHang = await prisma.orders.findMany({
            where:{
                order_cus_id:user_id
            },
            include:{
                images:{
                    where:{
                        img_status:1,
                    },
                    select:{
                        img_id:true,
                        img_path:true
                    }
                },
                services:{
                    select:{
                        service_name:true,
                        service_detail: true
                    }
                }
            }
        });
        res.status(200).send(donHang);
    }catch(err){
        res.status(500).send(err);
    }
}
let createDonHang = async(req,res)=>{
    try{
        let user_id = req.user.user_id;
        let orderInfo = req.body;
        if(orderInfo.service_id == null){
            res.status(400).send("Vui lòng nhập mã dịch vụ");
            return;
        }
        let serviceInfo = await prisma.services.findFirst({
            where:{
                service_id: orderInfo.service_id
            }
        })
        if(serviceInfo == null){
            res.status(400).send("Mã dịch vụ không hợp lệ");
            return;
        }
        if(orderInfo.order_working_date == "" || orderInfo.order_working_date == null){
            res.status(400).send("Vui lòng nhập thời gian bắt đầu công việc");
            return;
        }
        let timeZoneOffset = 7 * 60; // Điều chỉnh thời gian theo phút
        let now = new Date();
        now.setMinutes(now.getMinutes() + timeZoneOffset);
        orderInfo.order_create_date = new Date(now);
        
        // prisma bị lỗi không nhận thời gian theo string. Dùng Hàm new Date để convert sang date GMT+0 rồi xử lý sang GMT +7
        orderInfo.order_working_date = new Date(orderInfo.order_working_date);
        orderInfo.order_working_date.setMinutes(orderInfo.order_working_date.getMinutes() + timeZoneOffset);
        orderInfo.order_working_date = new Date(orderInfo.order_working_date);
        // 
        orderInfo.order_price = serviceInfo.service_price;
        orderInfo.order_cus_id = user_id;
        orderInfo.order_status = 0;
        let data = await prisma.orders.create({data:orderInfo});
        res.status(200).send(data);
    }catch(err){
        res.status(500).send(err);
    }
}
    let cancelDonHang = async(req,res) => {
        try {
            let order_id = Number(req.params.id);
            let user_id = req.user.user_id;
            //  kiểm tra thông tin đơn hàng
            let orderData = await prisma.orders.findFirst({
                where:{
                    order_id: order_id
                }
            })
            if(orderData == null){
                res.status(404).send("Không tìm thấy mã đơn hàng");
                return;
            }
            if(orderData.order_cus_id != user_id){
                res.status(401).send("Chỉ có người đặt đơn mới được phép huỷ!");
                return;
            }
            if(orderData.order_status == 2){
                res.status(400).send("Không được huỷ đơn hàng đã hoàn thành");
                return;
            }
            let data = await prisma.orders.update({
                data:{
                    order_status: 3
                },
                where:{
                    order_id
                }
            })
            res.status(200).send(data);
        } catch (error) {
            res.status(500).send(error);
        }
    }
module.exports = {getDonHang,createDonHang,cancelDonHang}
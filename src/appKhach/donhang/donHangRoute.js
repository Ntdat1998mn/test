const express = require("express");
const { checkToken } = require("../../middleWare/auth/jwt");
const { getDonHang, createDonHang, cancelDonHang } = require("./donHangController");
const donHangRoute = express.Router();

donHangRoute.get("/danh-sach-don-hang-app-khach",checkToken,getDonHang)
donHangRoute.post("/tao-don-hang",checkToken,createDonHang);
donHangRoute.delete("/huy-don-hang/:id",checkToken,cancelDonHang)

module.exports = donHangRoute;
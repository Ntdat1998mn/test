const express = require("express");
const { userList, login, register, update } = require("./userController");
const { checkToken } = require("../../middleWare/auth/jwt");
const userRoute = express.Router();

// Đăng Nhập
userRoute.post("/login",login);
// Đăng Ký
userRoute.post("/register",register);
// Đổi thông tin
userRoute.post("/update/:id",checkToken,update);
// 
userRoute.get("/user-list",userList);

module.exports = userRoute;
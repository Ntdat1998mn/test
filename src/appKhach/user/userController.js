const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();
const { createToken } = require("../../middleWare/auth/jwt");
const moment = require("moment");
//  Đăng Nhập
let login = async (req, res) => {
  try {
    let { username, password } = req.body;
    let user = await prisma.user_app_khach.findFirst({
      where: {
        user_username: username,
      },
    });
    if (user) {
      if (user.user_status !== 1) {
        res.status(400).send("Tài khoản bị khoá");
        return;
      }
      if (user.user_password == password) {
        user.user_password = "";
        token = createToken(user);
        data = { ...user, token };
        res.status(200).send(data);
      } else {
        res.status(400).send("Mật khẩu không chính xác");
      }
    } else {
      res.status(400).send("Tài khoản không chính xác");
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
let register = async (req, res) => {
  try {
    let user = req.body;
    if (user.user_username.length < 6 || user.user_username == null) {
      res.status(400).send("Tài khoản phải có ít nhất 6 ký tự");
      return;
    }
    if (user.user_password.length < 8 || user.user_password == null) {
      res.status(400).send("Mật khẩu phải có ít nhất 8 ký tự");
      return;
    }
    let checkUsername = await prisma.user_app_khach.findFirst({
      where: {
        user_username: user.user_username,
      },
    });
    if (checkUsername) {
      res.status(400).send("Tài khoản đã tồn tại");
      return;
    }
    let now = moment().format("YYYY-MM-DD hh:mm:ss");
    now = new Date(now);
    user.create_date = now;
    user.user_type = "user";
    let result = await prisma.user_app_khach.create({ data: user });
    res.status(201).send(result);
  } catch (error) {
    res.status(500).send(error.mesage);
  }
};
let update = async (req, res) => {
  try {
    let id = req.params.id;

    let userData = req.body;
    let userInfo = req.user;
    if (id != userInfo.user_id) {
      res.status(400).send("Id User không chính xác");
      return;
    }
    let data = await prisma.user_app_khach.update({
      data: userData,
      where: {
        user_id: Number(id),
      },
    });
    res.status(200).send(data);
  } catch (error) {
    res.status(500).send(error);
  }
};
let userList = async (req, res) => {
  let data = await prisma.user_app_khach.findMany();
  res.send(data);
};
module.exports = { userList, login, register, update };

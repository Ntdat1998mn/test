const express = require("express");
const userRoute = require("./user/userRoute");
const dangnhapbanggoogleRoute = require("./dangnhapbanggoogle/dangnhapbanggoogleRoute");

const appKhachRoute = express.Router();

appKhachRoute.use("/user", userRoute);
appKhachRoute.use("/user", dangnhapbanggoogleRoute);

module.exports = appKhachRoute;

const {PrismaClient} =   require('@prisma/client') 
const prisma = new PrismaClient()


//  Đăng Nhập
let getDichVu = async(req,res) => {
    try {
        let data = await prisma.services.findMany({
            where:{
                service_status: 1
            }
        });
        res.status(200).send(data);
    } catch (error) {
        res.status(500).send(error)
    }
}
module.exports = {getDichVu}
const express = require("express");
const { getDichVu } = require("./dichVuController");
const { checkToken } = require("../../middleWare/auth/jwt");

const dichVuRoute = express.Router();

// Lấy danh sách dịch vụ
dichVuRoute.get("/danh-sach-dich-vu",checkToken,getDichVu);

module.exports = {dichVuRoute};
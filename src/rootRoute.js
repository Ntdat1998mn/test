const express = require("express");
const appKhachRoute = require("./appKhach/appKhachRoute");
const { dichVuRoute } = require("./appKhach/dichvu/dichVuRoute");
const donHangRoute = require("./appKhach/donhang/donHangRoute");

const rootRoute = express.Router();

rootRoute.use("/app-khach",appKhachRoute);
rootRoute.use("/dich-vu",dichVuRoute);
rootRoute.use("/don-hang",donHangRoute);
rootRoute.use("/app-tho",(req,res)=>{
    res.send("hello");
});


module.exports = rootRoute;
const express = require("express");
const cors = require("cors");
const rootRoute = require("./rootRoute");
const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static("."));

app.use("/api", rootRoute);
app.use("/test", (req, res) => {
  res.send("hello");
  return;
});

const PORT = 3010;
app.listen(PORT);

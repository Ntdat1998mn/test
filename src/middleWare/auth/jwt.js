const jwt = require('jsonwebtoken');

const createToken = (data) => {
    let token = jwt.sign(data,"intelErp",{expiresIn:"1day"});
    return token;
}
const checkToken = (req,res,next) => {
    try{
        const {token} = req.headers;
        let data = jwt.verify(token,'intelErp');
        if(data){
            req.user = data;
            next();
        }
    }catch(err){
        res.status(401).send(err);
    }
}
module.exports = {createToken,checkToken};